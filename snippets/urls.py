from django.conf.urls import url, include, patterns
from rest_framework.routers import DefaultRouter

from . import views

#Create router an register our viewsets with it

router = DefaultRouter()
router.register(r'snippets', views.SnippetViewSet)
router.register(r'users', views.UserViewSet)

#The API urls are now determined automatically by the router
#Additionally we include the login url for the browsable api


urlpatterns = patterns('',
	url(r'^', include(router.urls)),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

	)
