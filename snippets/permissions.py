from rest_framework import permissions

class IsOwnerOrReadOnly(permissions.BasePermission):
	'''
	Custom permession to only allow owner of an object to edit it
	'''
	def has_object_permissions(self, request, view, obj):
		#Read permessions are allowed to any request
		#So we will allow GET, HEAD or OPTIONS requests
		if request.method in permissions.SAFE_METHODS:
			return True

		#write permession is only allowed to the owner of the snippet
		return obj.owner == request.user
