from django.contrib.auth.models import User

from rest_framework import generics
from rest_framework import mixins
from rest_framework.views import APIView
from rest_framework.decorators import api_view, detail_route
from rest_framework import status
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.reverse import reverse
from rest_framework import renderers
from rest_framework import viewsets
from .models import Snippet
from .serializers import SnippetSerializer, UserSerializer
from snippets.permissions import IsOwnerOrReadOnly


class SnippetViewSet(viewsets.ModelViewSet):
	'''
	This viewset provides automatically list, create, retrieve,
	update and destroy actions

	Additionally, we also provide an extra hightlight action
	'''

	queryset = Snippet.objects.all()
	serializer_class = SnippetSerializer
	permissions_classes = (permissions.IsAuthenticatedOrReadOnly,
		IsOwnerOrReadOnly,)

	@detail_route(renderer_classes=(renderers.StaticHTMLRenderer,))
	def highlight(self, request, *args, **kwargs):
		snippet = self.get_object()
		return Response(snippet.highlighted)

	def perform_create(self, serializer):
		serializer.save(owner=self.request.user)


class UserViewSet(viewsets.ReadOnlyModelViewSet):
	'''
	This viewset provides automatically list and detail actions
	'''
	queryset = User.objects.all()
	serializer_class = UserSerializer
